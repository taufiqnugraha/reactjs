import React from 'react';
import '../css/Skills.css';
import { Button } from 'semantic-ui-react';

const Skills = ({screenWidth, navigate}) => {

  const skills = {
    'Foreign Language': [
      {
        name: 'Bahasa',
        proficient: true
      }
    ],
    'Programming Languages': [
      {
        name: 'JavaScript',
        proficient: true
      },
      {
        name: 'HTML5',
        proficient: true
      },
      {
        name: 'CSS3',
        proficient: true
      },
      {
        name: 'PHP',
        proficient: true
      },
      {
        name: 'C#',
        proficient: true
      },
      {
        name: 'Python',
        proficient: false
      },
      
    ],
    Frontend: [
      {
        name: 'jQuery',
        proficient: true
      },
      {
        name: 'Bootstrap',
        proficient: true
      },
      
    ],
    
    Backend: [
      {
        name: 'REST APIs',
        proficient: true
      },
      {
        name: 'Pusher',
        proficient: true
      },
      {
        name: 'Socket.io',
        proficient: true
      },
      {
        name: 'Firebase',
        proficient: false
      }
    ],
    Database: [
      {
        name: 'Mysql',
        proficient: true
      }
    ],
    DevOps: [
      {
        name: 'Git',
        proficient: true
      },
    ],
    'Other Tools': [
      {
        name: 'Trello',
        proficient: true
      }
    ]
  }

  return (
    <div id="skills" onMouseOver={navigate}>
      <div className={screenWidth > 830 ? "category" : "category-mobile"} >
        <h1 className={screenWidth > 830 ? 'skills-header' : 'skills-header-mobile'}><span className="section_header">Skills</span></h1>
        <div className="legend" >
          <Button.Group size="medium">
            <Button className="non-click" color="green">Proficient</Button>
            <Button className="non-click" color="violet">Basic</Button>
          </Button.Group>
        </div>
      </div>
      {Object.keys(skills).map(category => (
        <div key={category} className={screenWidth > 830 ? "category" : "category-mobile"} >
          <h3 className={screenWidth > 830 ? "category-name" : "category-name-mobile"} >{category}</h3>
          <div className="skills-container" >
            {skills[category].map(skill => {
              return (
                <Button key={skill.name} className="non-click skill-item" inverted color={skill.proficient ? "green" : "violet"}>
                  {skill.name}
                </Button>
              )
            })}
          </div>
        </div>
      ))}
    </div>
  )
}

export default Skills;