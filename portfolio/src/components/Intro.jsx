import React from 'react';
import '../css/Intro.css';
import { Responsive } from 'semantic-ui-react';

const Intro = ({navigate}) => {
  return (
    <div id="intro" onMouseOver={navigate}>
      <div className="heading">
        <Responsive maxWidth={800}>
          <h1 className="titles">
           Taufiq Nugraha
          </h1>
          <h1 className="titles">
            LIFELONG LEARNER
          </h1>
          <h1 className="titles">
            TECH ENTHUSIAST
          </h1>

        </Responsive>
        <Responsive minWidth={801}>
          <h1 className="titles">
            Learn React
          </h1>
          <div className="quote">
            " ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ "
          </div>
        </Responsive>
      </div>
    </div>
  )
}

export default Intro;