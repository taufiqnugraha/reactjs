import React from 'react';
import '../css/Bio.css';
import { Icon } from 'semantic-ui-react';
import profile from '../assets/profile.jpg';

const Bio = () => {
  return (
    <div id="biography">
      <h1><span className="section_header">About Me</span></h1>
      <img src={profile} className="profile" alt="Taufiq Nugraha" height="250px" />
      <div className="text-intro loc">
        <Icon fitted={true} size="large" color="red" name="map marker alternate" /> <span className="location">Padang, IND</span>
      </div>
      <div className="text-intro">
        Saya Taufiq Nugraha,
      </div>
      <div className="text-intro">
       Lorem ipsum dolor sit amet consectetur adipisicing elit. Vero tenetur sit aspernatur corporis omnis eos dolore corrupti, dolores culpa nesciunt, molestiae optio. Nihil adipisci quae deleniti. Ipsam aliquid molestias debitis?
      </div>
      <div className="text-intro"><strong>
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptates doloribus aut voluptate temporibus, accusantium itaque quisquam impedit. Velit reiciendis doloremque dolor, saepe, ipsum nihil minus, temporibus consequatur quibusdam tempore dolore.
      </strong></div>
    </div>
  )
}

export default Bio;