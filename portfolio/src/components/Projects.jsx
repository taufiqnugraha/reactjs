import React from 'react';
import '../css/Projects.css';
import { Icon, Reveal, Button, Card, Image } from 'semantic-ui-react';
import jobmate from '../assets/project.JPG';
import grutty from '../assets/dua.JPG';

const Projects = ({navigate, screenWidth}) => {

  const projects = [
    {
      name: 'Tanah Airku',
      date: 'July 2017',
      img: jobmate,
      desc: 'Sebuah aplikasi dan juga media pembelajaran kebudayaan indonesia dengan cara interaktif untuk masyarakat umum maupun turis lokal ataupun internasional untuk mempelajari kebudayaan indonesia menggunakan teknologi aughmented reality dengan marker pada buku yang telah disediakan. Sehingga diharapkan warga indonesia ataupun turis mancanegara bisa lebih mengenal dan mencintai kebudayaan indonesia.',
      stacks: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cupiditate iusto laudantium veritatis doloremque exercitationem architecto alias placeat at dolorum debitis commodi quisquam sapiente, perspiciatis eos officia deleniti omnis reiciendis. Dolorum!',
      github: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cupiditate iusto laudantium veritatis doloremque exercitationem architecto alias placeat at dolorum debitis commodi quisquam sapiente, perspiciatis eos officia deleniti omnis reiciendis. Dolorum!',
      other: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cupiditate iusto laudantium veritatis doloremque exercitationem architecto alias placeat at dolorum debitis commodi quisquam sapiente, perspiciatis eos officia deleniti omnis reiciendis. Dolorum!'
    },
    {
      name: 'ERP Grutty',
      date: 'September 2018',
      img: grutty,
      desc: 'Lorem',
      stacks: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cupiditate iusto laudantium veritatis doloremque exercitationem architecto alias placeat at dolorum debitis commodi quisquam sapiente, perspiciatis eos officia deleniti omnis reiciendis. Dolorum!',
      github: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cupiditate iusto laudantium veritatis doloremque exercitationem architecto alias placeat at dolorum debitis commodi quisquam sapiente, perspiciatis eos officia deleniti omnis reiciendis. Dolorum!',
      other: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cupiditate iusto laudantium veritatis doloremque exercitationem architecto alias placeat at dolorum debitis commodi quisquam sapiente, perspiciatis eos officia deleniti omnis reiciendis. Dolorum!'
    },
    
  ]

  return (
    <div id="projects" onMouseOver={navigate}>
      <h1><span className="section_header">My Projects</span></h1>
      <Card.Group className="projects-container" stackable={true} itemsPerRow={screenWidth > 1100 ? 3 : 2}>
        {projects.map((project, index) => (
          <Card key={project.name} color={index%2 ? "teal" : "orange"}>
            <Card.Content header={project.name} meta={project.date} />
            <Reveal animated="move up">
              <Reveal.Content visible>
                <Image as="img" width="40%" height="40%" fluid={true} src={project.img} className="project-img" />
              </Reveal.Content>
              <Reveal.Content hidden>
                <Card.Description className="project-description">
                  {project.desc}
                  <br />
                  <br />
                  <span>
                    <u>Tech Stack</u>: {project.stacks}
                  </span>
                </Card.Description>
              </Reveal.Content>
            </Reveal>
            <Card.Content extra textAlign="center">
              <a href={project.github} rel="noopener noreferrer" target="_blank">
                <Button color="black" size="tiny" icon>
                  <Icon name="github" size="large" />
                </Button>
              </a>
              {project.other && (
                <a href={project.other} rel="noopener noreferrer" target="_blank">
                  <Button color="green" size="tiny" icon>
                    <Icon name="arrow right" size="large" />
                  </Button>
                </a>
              )}
            </Card.Content>
          </Card>
        ))}
      </Card.Group>
    </div>
  )
}

export default Projects;