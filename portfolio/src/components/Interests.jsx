import React from 'react';
import '../css/Interests.css';
import { Button } from 'semantic-ui-react';

const Interests = () => {

  const interests = ['Laravel', 'JS', 'Data Mining', 'Tableau', 'D3', 'Augmented Reality', 'Virtual Reality']

  return (
    <div className="interests">
      <h1><span className="section_header">Interests</span></h1>
      <div className="interests-container">
        {interests.map(i => (
          <Button className="interest-item non-click" inverted size="medium" key={i} color="green">
            {i}
          </Button>
        ))}
      </div>
    </div>
  )
}

export default Interests;