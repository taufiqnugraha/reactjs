import React, { Component } from 'react'

export default class Home extends Component {

    constructor(props) {
        super(props)

        this.state = {
            items: []
        }
    }

    componentDidMount() {
        fetch('https://randomuser.me/api/?results=10')
        .then(res => res.json())
        .then(parsedJSON => parsedJSON.results.map(data => ({
            id : `${data.id.name}`,
            firstName : `${data.name.first}`,
            lastName : `${data.name.last}`,
            location: `${data.location.state},${data.nat}` ,
            thumnail: `${data.picture.large}`,
            nationality: `${data.nat}`
        })))
        .then(items => this.setState({
            items,
            isLoaded: false
        }))
        .catch(error => console.log('parsing failed', error))
    }

    render() {
        const {items} = this.state;
        return (
            <div className="boxWhite">
                <h2> Random User</h2>
                {
                    items.length > 0 ? items.map(item => {
                        const {id, firstName, lastName, location, thumnail, nationality} = item
                        return (
                            <div key={id} className="bgCircle">
                                <center>
                                    <img src={thumnail} alt={firstName} className="Circle" />
                                </center>
                                <br></br>
                                <div className="ctr">
                                    {firstName} {lastName} 
                                    <br></br>
                                    {location} ~ {nationality}
                                </div>
                            </div>
                        )
                    }): null
                }
            </div>
        )
    }
}
