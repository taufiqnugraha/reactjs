const themes = {
    luna: {
        background : "#1c4958",
        foreground: "#aiccde",

        accent1: "#f4c8a5",
        accent2: "#b47368",
        accent3: "#923621",

        text1: "#c6cf63",
        text2: "#808186"
    },

    mars: {
        background : "#a53237",
        foreground: "#f66f40",

        accent1: "#f8986d",
        accent2: "#9c4952",
        accent3: "#f66f40",

        text1: "#f5e5e1",
        text2: "#354f55"
    }
};

export default themes;