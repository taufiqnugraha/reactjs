import React, { Component } from 'react'
import {Route, NavLink, HashRouter} from "react-router-dom";
import Home from "./Home";
import Australia from "./Australia";
import Turkey from "./Turkey";


class Main extends Component {
    // render() {
    //     return (
    //         <HashRouter>
    //         <div>
    //             <h1 className="title">Fething Data API</h1>
    //             <ul className="header">
    //                 <li><NavLink exact to="/">Home</NavLink></li>
    //                 <li><NavLink to="/australia">Australia</NavLink></li>
    //                 <li><NavLink to="/turkey">Turkey</NavLink></li>
    //             </ul>
    //         </div>
    //         <div className="content">
    //             <Route exact path="/" component={Home}></Route>
    //             <Route exact path="/australia" component={Australia}></Route>
    //             <Route exact path="/turkey" component={Turkey}></Route>
    //         </div>
    //         </HashRouter>
    //     )
    // 

  constructor(props) {
      super(props)
  
      this.state = {
        active: "Home"
      }
  }

  clickName(menu){
    this.setState ({
        active: menu
    })  
    console.log(menu)
  }

  
  render() {
      
    return (
        
      <div id="app" >
        <nav className="nav">{["home", "about","service"].map((menu, index) => {
            let style = "menu";
            if(this.props.active === menu) {
                style = `${style} is-active`;
            }
        return (<button onClick={this.clickName.bind(this, menu)} key={index}>{menu}</button>)
            
        })}

        </nav>
        <div>
        Anda memilih <button className="btn btn-secondary">{this.state.active}</button>
        </div>
      </div>
    );
  }
}

export default Main
