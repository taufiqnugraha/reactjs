import React from "react";

import { kitAttribution as KitAttribution, kitImage as KitImage, kitCarousel as KitCarousel, kitCarouselItem as KitCarouselItem } from "../kit";

export default function PosterCarousel(props) {
  const carouselStyle = {
    marginBottom: "15px"
  };

  const renderItem = (destination, index) => {
    return (
      <KitCarouselItem key={index}>
        <KitImage src={destination.poster} fluid />
      </KitCarouselItem>
    );
  };

  return (
    <React.Fragment>
      <KitCarousel
        style={carouselStyle}
        fade={true}
        indicators={false}
        activeIndex={props.destinationIndex}
        onSelect={(key, event) => props.setDestinationIndex(key)}
        interval={null}
      >
        {props.destinations.map((destination, index) =>
          renderItem(destination, index)
        )}
      </KitCarousel>
      <KitAttribution style={carouselStyle}>
        Poster Art by &nbsp;
        <a
          target="_blank"
          rel="noopener noreferrer"
          href="https://www.stevethomasart.com/spacetravel"
        >
          Space Travel
        </a>
      </KitAttribution>
    </React.Fragment>
  );
}
