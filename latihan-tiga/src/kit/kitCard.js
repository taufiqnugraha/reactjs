import React, {Component} from 'react';
import styled from "styled-components";
import { Card } from "react-bootstrap";

const StyleBootstrapCard = styled(Card)`
    background-color:  ${props => props.theme.accent1};
    color:  ${ props => props.theme.text1};
    box-shadow: 0 0 1px 2px #fdb813, 0 0 3px 4px #f8986d;
    border-radius: 7px 25px;
    border-color: ${ props => props.theme.accent3};
    border-style: solid;
    border-width: 1px;
    font-family: "Nunito", sans-serif;
    margin-bottom: 20px;
`;

export class kitCard extends Component {
    render() {
        const { ...props } = this.props;
        return <StyleBootstrapCard {...props} />
    }
}
const StyleBootstrapCardSubtitle = styled(Card.Subtitle)`
    border-bottom: dotted 1px;
    border-color:  ${props => props.theme.accent3};
    margin-bottom: 10px;
`;

export class kitCardSubtitle extends Component {
    render() {
        const {...props} = this.props;
        return <StyleBootstrapCardSubtitle {...props} />
    }
}

export const kitCardBody = styled(Card.Body)``;
export const kitCardTitle = styled(Card.Title)``;
export const kitCardFooter = styled(Card.Footer)``;
