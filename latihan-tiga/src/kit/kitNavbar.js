import React, { Component } from 'react';
import styled from "styled-components";
import {Navbar} from 'react-bootstrap';

const StyledBootsrapNavbar = styled(Navbar)`
  background-color: ${props => props.theme.background};
  box-shadow: 0 0 1px 2px #fdb813, 0 0 3px 40px #f8986d;
  display: flex;
  flex-direction: horizontal;
  justify-content: space-between;
  font-family: "Nunito", sans-serif;
`;

export class kitNavbar extends Component {
    render() {
      const {...props} = this.props;
      return <StyledBootsrapNavbar fixed="top" {...props} />       
    }
}
export const kitNavbarBrand = styled(Navbar.Brand)`
&&& {
    display : flex;
    flex-direction: horizontal;
    justify-content: space-between;
    color:  ${props => props.theme.foreground};
    text-shadow: 0 1px 2px rgba(0, 0, 0 , 0 .75);
    font-family: "Orbitron", sans-serif;
    font-size: 20px;
    padding-left: 10px;
}
`;

export const kitLogo = styled.div`
  margin-top: 8px;
  margin-left: 5px;
  width: 12px;
  height: 12px;
  border-radius: 50%;
  background-color: #ffff00;
  box-shadow: 0 0 10px 5px #fdb813, 0 0 15px 20px #f8986d;
`;
