import styled from 'styled-components';
import {Carousel} from "react-bootstrap";

export const kitCarousel = styled(Carousel)`
    border-color: ${props => props.theme.accent3};
    border-style: solid;
    border-width: 1px;
`;


export const kitCarouselItem = styled(Carousel.Item)``;