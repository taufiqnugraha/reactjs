import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/App';

const DESTINATION = [{
    theme: "luna",
    title: "luna",
    distance: "240 Thousand Miles",
    travelTime: "2 Days",
    cryoSleep: "None",
    transport: [
        "Blue Origin New Amstrong"
    ],
    arrivalSites: [
        "Luna City"
    ],
    header: "A Great Vocation for the Whole Family",
    blurb: "There is so much to do!",
    siteMap: "",
    poster: "https://uploads.codesandbox.io/uploads/user/13a9d4ab-104d-4152-bf41-621a9097470b/JBWA-luna.jpg"
}, 
{
    theme: "mars",
    title: "mars",
    distance: "240 Thousand Miles",
    travelTime: "2 Days",
    cryoSleep: "None",
    transport: [
        "Blue Origin New Amstrong"
    ],
    arrivalSites: [
        "mars City"
    ],
    header: "A Great Vocation for the Whole Family",
    blurb: "There is so much to do!",
    siteMap: "",
    poster: "https://uploads.codesandbox.io/uploads/user/13a9d4ab-104d-4152-bf41-621a9097470b/JBWA-luna.jpg"
}]

// let items= ["home", "about","service"];

ReactDOM.render(<App destinations={DESTINATION}/>, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
// serviceWorker.unregister();
