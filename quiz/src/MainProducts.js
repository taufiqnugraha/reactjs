import React, { Component } from 'react'
import Products from './Products';

export default class MainProducts extends Component {
    constructor(props) {
        super(props)
        this.state = {
            total : 0,
        };
        this.addTotal = this.addTotal.bind(this);
    }

    addTotal(price) {
        this.setState({
            total: this.state.total + price,
        });
    }

    render() {
        const services = this.props.products.map((product, index) => {
            return <Products product={product} addTotal={this.andTotal} key={index} />
        })

        return (
            <form id="app">
                <h1>Pesanan</h1>
                <ul>
                    {services}
                </ul>
                <div className="total">
                    total <span>Rp. {this.state.total}</span>
                </div>
            </form>
        )
    }
}
