import './App.css';

import React, { Component } from 'react'

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      searchTerms : ""
    }
    this.handleChange = this.handleChange.bind(this);
  }
  
  handleChange(e) {
    this.setState({
      searchTerms: e.target.value
    })
  }


  render() {
    const {products} = this.props;
    const searchTerms = this.state.searchTerms.trim().toLowerCase();
    let filteredProducts = products;

    if(searchTerms.length > 0) {
      filteredProducts = products.filter((product) => {
        console.log(product.name.toLowerCase().indexOf(searchTerms));
        return product.name.toLowerCase().indexOf(searchTerms) !== -1
      })
    }
    return (
      <form id="app">  
        <div className="search">
          <input type="text" placeholder="products" onChange={this.handleChange}></input>
        </div>
        <ul>
          {filteredProducts.map((product, index) => {
            return (
              <li key={index}>
                {product.name}
              </li>
            )
          })}
        </ul>
      </form>
    )
  }
}


export default App;
