import React, { Component } from 'react';
import ControlItem from "./ControlItem";
import List from "./List";
import "./style.css";

export default class Gallery extends Component {
    constructor(props) {
        super(props);
        this.state = {
            layout: "list"
        };
        this.switchLayout = this.switchLayout.bind(this);
    }

    switchLayout(layout) {
        this.setState({
            layout,
        });
    }

    render() {
        const { layout } = this.state;
        const { products } = this.props;
        return (
            <div className="app">
                <div className="controls">
                    <ControlItem 
                        layout="list" 
                        activelayout={layout}
                        onDepressed={this.switchLayout}>

                     </ControlItem>
                     <ControlItem 
                        layout="grid" 
                        activelayout={layout}
                        onDepressed={this.switchLayout}>

                     </ControlItem>
                </div>
                
                <List layout={layout} products={products}>
                    
                </List>
            </div>
        )
    }
}
