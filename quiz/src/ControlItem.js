import React, { Component } from 'react'

const titleize = (str) => str.charAt(0).toUpperCase() + str.substring(1, str.length);

const ControlItem = ({ layout, activelayout, onDepressed }) => {
    console.log(layout, activelayout, onDepressed)
    
    return (
    <button href="javascript:void()"
        className={layout === activelayout ? "active" : ""} 
        onClick={onDepressed.bind(this, layout)}
    >
        {titleize(layout)} View 
    </button>   
    )
}

export default ControlItem;