import React from 'react';
import ReactDOM from 'react-dom';
import './style.css';
import Gallery from './Gallery';
// import * as serviceWorker from './serviceWorker';

// const products = [
//     {
//         name: "Bali",
//         active: true,
//     },
//     {
//         name: "Jakarta",
//         active: true,
//     },
//     {
//         name: "Manado",
//         active: true,
//     },
//     {
//         name: "Lombok",
//         active: true,
//     },
//     {
//         name: "Sumba",
//         active: true,
//     },
//     {
//         name: "Raja Ampat",
//         active: true,
//     }
// ]

// const products = [
//     {
//         name: "Cappucino",
//         price: 30000,
//         active: true,
//     },
//     {
//         name: "Late",
//         price: 40000,
//         active: true,
//     },
//     {
//         name: "GreenTea",
//         price: 35000,
//         active: true,
//     },
//     {
//         name: "Tea",
//         price: 10000,
//         active: true,
//     },
//     {
//         name: "Juice",
//         price: 40000,
//         active: true,
//     },
//     {
//         name: "Milk",
//         price: 15000,
//         active: true,
//     }
// ]

const products = [
{
    name: 'Cappucino',
    image: {
    medium: 'https://i.imgur.com/Tp3FP7R.jpg',
    small: 'https://i.imgur.com/cm1QpWn.jpg',
    }
},
{
    name: 'Green Tea Latte',
    image: {
    medium: 'https://i.imgur.com/aUGBfvI.jpg',
    small: 'https://i.imgur.com/mWsFDbO.jpg',
    }
}, {
    name: 'Fish and Chips',
    image: {
    medium: 'https://i.imgur.com/ItOox1N.jpg',
    small: 'https://i.imgur.com/keKToBD.jpg',
    }
}, {
    name: 'Tuna Sandwich',
    image: {
    medium: 'https://i.imgur.com/OrrDIBd.jpg',
    small: 'https://i.imgur.com/vKhChve.jpg',
    }
}, {
    name: 'Mineral Water',
    image: {
    medium: 'https://i.imgur.com/9MGbQLR.jpg',
    small: 'https://i.imgur.com/WIzJWes.jpg',
    }
}, {
    name: 'Frence Fries',
    image: {
    medium: 'https://i.imgur.com/6CEZAgt.jpg',
    small: 'https://i.imgur.com/9aKYsOl.jpg',
    }
},
];

ReactDOM.render(<Gallery  products={products}/>, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
// serviceWorker.unregister();
