import React, { Component } from 'react';

const List = ({ layout, products }) => (
    <ul className={layout}> {products.map((product, index) => {
        return (
            <li>
                <img src={layout === "list" ? product.image.small : product.image.medium } />
                {
                   layout == "list" ? <p>{product.name}</p> : ""
                }
            </li>
        );
    })} </ul>
)

export default List;
